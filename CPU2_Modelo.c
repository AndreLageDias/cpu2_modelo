/* ========================================================

 Trabalho III - C�digo do Modelo
 T�cnicas de Comando e Instrumenta��o em Eletr�nica de Pot�ncia
 P�s-Gradua��o em Engenharia El�trica
 Universidade Federal de Minas Gerais
 Autores: Andr� Lage Almeida Dias e Yasmine Neves Maia

 ==========================================================*/

#include "F28x_Project.h"

// Function Prototypes
interrupt void Modelo(void);

// Variables
float iah=0, ibh=0, ich=0, i1=0, i2=0, di1=0, di2=0;
float da=0, db=0, dc=0;
float Vcc=48, L=1e-3, R=10e-3, k1=0, k2=0, k3=0;

void main(void)
{
   // Initialize System Control
   InitSysCtrl();

   // Clear all interrupts and initialize PIE control registers
   DINT;
   InitPieCtrl();

   // Disable CPU interrupts and clear all CPU interrupt flags
   IER = 0x0000;
   IFR = 0x0000;

   // Initialize the PIE vector table
   InitPieVectTable();

   // Map ISR functions
   EALLOW;
   PieVectTable.IPC1_INT = &Modelo;        // IPC2 interrupt
   EDIS;

   IpcRegs.IPCCLR.bit.IPC2 = 1;
   PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;

   //enable PIE interrupts
   PieCtrlRegs.PIEIER1.bit.INTx14 = 1;     // Enable IPC2 ISR
   PieCtrlRegs.PIEIFR1.bit.INTx13 = 1;     // Force first IPC1 transfer

   // Enable global interrupts and higher priority real-time debug events:
   IER |= M_INT1;                          // Enable group 1 interrupts
   EINT;                                   // Enable Global interrupt INTM
   ERTM;                                   // Enable Global real-time interrupt DBGM

   // Informar para a CPU1 que a CPU2 est� pronta
   IpcRegs.IPCSET.bit.IPC17 = 1;          // Set IPC17 to release CPU1

   // C�lculo dos ganhos constantes
   k1 = 1/(2*L);
   k2 = Vcc*0.5;
   k3 = 2*R;

   while(1)
   {

   }
}

interrupt void Modelo(void)
{
   // Receber os sinais PWM das fases a, b e c enviados pela CPU1
   da = ( (Uint16) IpcRegs.IPCRECVDATA - 0.5 ) * 2;
   db = ( (Uint16) IpcRegs.IPCRECVCOM - 0.5 ) * 2;
   dc = ( (Uint16) IpcRegs.IPCRECVADDR - 0.5 ) * 2;

   // C�lculo das equa��es diferenciais do modelo
   di1 = k1 * (da*k2 - k3*i1 + R*i2 + L*di2 - db*k2);
   di2 = k1 * (db*k2 - k3*i2 + R*i1 + L*di1 - dc*k2);

   // C�lculo das integrais das diferenciais das correntes
   i1 += di1*4e-6;
   i2 += di2*4e-6;

   // C�lculo das correntes de linha
   iah = i1;
   ibh = i2 - i1;
   ich = - i2;

   // Enviar as correntes ia e ib em formato inteiro para CPU1
   IpcRegs.IPCSENDDATA = (int) (iah*100);
   IpcRegs.IPCSENDCOM = (int) (ibh*100);
   IpcRegs.IPCSET.bit.IPC0 = 1;            // Set IPC1 bit for CPU1

   // Return from interrupt
   IpcRegs.IPCACK.bit.IPC1 = 1;            // Clear IPC2 bit
   PieCtrlRegs.PIEACK.all = PIEACK_GROUP1; // Acknowledge PIE group 1 to enable further interrupts
}
